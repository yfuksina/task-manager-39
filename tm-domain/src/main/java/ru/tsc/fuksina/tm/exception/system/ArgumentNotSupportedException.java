package ru.tsc.fuksina.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.exception.AbstractException;

public class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Argument `" + argument + "` not supported...");
    }

}
