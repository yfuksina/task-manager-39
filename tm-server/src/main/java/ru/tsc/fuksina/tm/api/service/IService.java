package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.api.repository.IRepository;
import ru.tsc.fuksina.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
