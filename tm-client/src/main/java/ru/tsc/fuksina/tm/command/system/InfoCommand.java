package ru.tsc.fuksina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.util.ByteUtil;

public final class InfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String DESCRIPTION = "Show system info";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        @NotNull final String freeMemoryFormat = ByteUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        @NotNull final String maxMemoryValue = ByteUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        @NotNull final String totalMemoryFormat = ByteUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVN: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = ByteUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVN: " + usedMemoryFormat);
    }

}
